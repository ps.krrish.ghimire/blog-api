<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register','AuthController@register');
Route::post('/login','AuthController@login');
Route::get('/user','AuthController@user');
Route::post('/logout','AuthController@logout');

Route::group(['prefix' => 'posts'], function(){
    Route::post('/','postController@store')->middleware('auth:api');
    Route::get('/{post}','postController@show');
    Route::post('/{post}/file','FileController@store');
    Route::patch('/{post}','postController@update')->middleware('auth:api');
    Route::get('/','postController@index');
    Route::delete('/{post}','postController@destroy')->middleware('auth:api');
});

Route::get('/post','postController@getIndividualPost')->middleware('auth:api');
Route::get('/privatePost','postController@getPrivatePost')->middleware('auth:api');
Route::group(['prefix' => '/posts'], function() {
    Route::post('/{post}/comment', 'CommentController@store')->middleware('auth:api');
});



